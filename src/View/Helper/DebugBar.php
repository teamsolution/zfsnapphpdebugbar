<?php

namespace ZfSnapPhpDebugBar\View\Helper;

use DebugBar\JavascriptRenderer;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Helper\HeadLink;
use Zend\View\Helper\HeadScript;

/**
 * @author Witold Wasiczko <witold@wasiczko.pl>
 */
class DebugBar extends AbstractHelper
{
    /**
     * @var JavascriptRenderer
     */
    protected $renderer;

    protected $customStyle;

    /**
     * @param JavascriptRenderer $renderer
     */
    public function __construct(JavascriptRenderer $renderer, $customStyle)
    {
        $this->renderer = $renderer;
        $this->customStyle = $customStyle;
        $this->lng = $this->getLng();
        $this->assetsPrefix = empty($this->lng) ? '' : '/'.$this->lng;

        if(MULTILANGUAGE){
            $baseUrl = $this->renderer->getBaseUrl();
            $baseUrl = $this->assetsPrefix.$baseUrl;

            $this->renderer->setBaseUrl($baseUrl);
        }
    }

    private function getLng(){
        $lng = '';
        if(MULTILANGUAGE){
            if (_APPLICATION_TYPE_ == 'Backend') {
                $lng = DEBUGBAR_BACKEND_LNG;
            } else {
                $lng = DEBUGBAR_FRONTEND_LNG;
            }
        }

        return $lng;
    }

    /**
     * @return void
     */
    public function appendAssets()
    {
        $this->appendScripts();
        $this->appendStyles();
    }

    /**
     * @return HeadScript
     */
    public function appendScripts()
    {
        $scripts = $this->renderer->getAssets('js');
        $headScript = $this->getView()->headScript();

        foreach ($scripts as $script) {
            $headScript->appendFile($this->assetsPrefix.$script);
        }
        return $headScript;
    }

    /**
     * @return HeadLink
     */
    public function appendStyles()
    {
        $styles = $this->renderer->getAssets('css');
        $headLink = $this->getView()->headLink();

        foreach ($styles as $style) {
            $headLink->appendStylesheet($this->assetsPrefix.$style);
        }
        
        $this->addCustomStyles($headLink);

        return $headLink;
    }

    private function addCustomStyles(&$headLink){
        $urlArgs = array('resource' => $this->customStyle);
        if(MULTILANGUAGE){
            $urlArgs['lng'] = $this->lng;
        }
        $css_url = $this->getView()->url('phpdebugbar-custom-resource', $urlArgs);
        $css_url = str_replace('/administracja', '', $css_url);
        
        $headLink->appendStylesheet($css_url);
    }

    /**
     * @return string
     */
    public function render()
    {
        return $this->renderer->render();
    }

    /**
     * @return string
     */
    public function renderHead()
    {
        return $this->renderer->renderHead();
    }

}
